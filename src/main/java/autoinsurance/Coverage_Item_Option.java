/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: uengine
 * License Type: Purchased
 */
package autoinsurance;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Coverage_Item_Option")
public class Coverage_Item_Option implements Serializable {
	public Coverage_Item_Option() {
	}
	
	@Column(name="ID", nullable=false)	
	@Id	
	@GeneratedValue(generator="AUTOINSURANCE_COVERAGE_ITEM_OPTION_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="AUTOINSURANCE_COVERAGE_ITEM_OPTION_ID_GENERATOR", strategy="native")	
	private int ID;
	
	@Column(name="Coverage_item_option_code", nullable=true, length=255)	
	private String coverage_item_option_code;
	
	@Column(name="Coverage_item_option_value", nullable=true, length=255)	
	private String coverage_item_option_value;
	
	@OneToOne(mappedBy="coverage_Item_Option", targetEntity=autoinsurance.Coverage_Item.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	private autoinsurance.Coverage_Item coverage_Item;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setCoverage_item_option_code(String value) {
		this.coverage_item_option_code = value;
	}
	
	public String getCoverage_item_option_code() {
		return coverage_item_option_code;
	}
	
	public void setCoverage_item_option_value(String value) {
		this.coverage_item_option_value = value;
	}
	
	public String getCoverage_item_option_value() {
		return coverage_item_option_value;
	}
	
	public void setCoverage_Item(autoinsurance.Coverage_Item value) {
		this.coverage_Item = value;
	}
	
	public autoinsurance.Coverage_Item getCoverage_Item() {
		return coverage_Item;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
