/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: uengine
 * License Type: Purchased
 */
package autoinsurance;

import org.metaworks.annotation.Range;
import org.metaworks.annotation.ServiceMethod;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Vehicle")
public class Vehicle implements Serializable {
	public Vehicle() {
    }

	@Column(name="ID", nullable=false)
	@Id
	@GeneratedValue(generator="AUTOINSURANCE_VEHICLE_VEHICLE_VIN_GENERATOR")
	@org.hibernate.annotations.GenericGenerator(name="AUTOINSURANCE_VEHICLE_VEHICLE_VIN_GENERATOR", strategy="native")
	private String vehicle_VIN;

	@ManyToOne(targetEntity=autoinsurance.Customer.class, fetch=FetchType.LAZY)
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})
	@JoinColumns({ @JoinColumn(name="CustomerID", referencedColumnName="ID", nullable=false) })
	private autoinsurance.Customer owns;

	@Column(name="Vehicle_type", nullable=true, length=255)
	private String vehicle_type;

	@Column(name="Vehicle_make", nullable=true, length=255)
	private String vehicle_make;

	@Column(name="Vehicle_model", nullable=true, length=255)
	private String vehicle_model;

	@Column(name="Vehicle_year", nullable=true, length=255)
	private String vehicle_year;

	@Column(name="Vehicle_primary_use", nullable=true, length=255)
	private String vehicle_primary_use;

	@Column(name="Vehicle_kept_zip_code", nullable=true, length=255)
	private String vehicle_kept_zip_code;

	@Column(name="Vehicle_ownership", nullable=true, length=255)
	private String vehicle_ownership;

	@OneToOne(mappedBy="vehicle", targetEntity=autoinsurance.Insurance_Quote.class, fetch=FetchType.LAZY)
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})
	private autoinsurance.Insurance_Quote is_created_for;

	public void setVehicle_VIN(String value) {
		this.vehicle_VIN = value;
	}

	public String getVehicle_VIN() {
		return vehicle_VIN;
	}

	public String getORMID() {
		return getVehicle_VIN();
	}

	public void setVehicle_type(String value) {
		this.vehicle_type = value;
	}

	@Range(options = {"Auto/Van/Truck/SUV", "Van", "Truck", "SUV"}, values = {"Auto/Van/Truck/SUV", "Van", "Truck", "SUV"})
	public String getVehicle_type() {
		return vehicle_type;
	}

	public void setVehicle_make(String value) {
		this.vehicle_make = value;
	}

	@Range(options = {"Volvo", "Audi", "Bentz", "SM5"}, values = {"Volvo", "Audi", "Bentz", "SM5"})
	public String getVehicle_make() {
		return vehicle_make;
	}

	public void setVehicle_model(String value) {
		this.vehicle_model = value;
	}

	@Range(options = {"S60", "S50", "S40", "S30"}, values = {"S60", "S50", "S40", "S30"})
	public String getVehicle_model() {
		return vehicle_model;
	}

	public void setVehicle_year(String value) {
		this.vehicle_year = value;
	}

	@Range(options = {"2015", "2014", "2013", "2012", "2011"}, values = {"2015", "2014", "2013", "2012", "2011"})
	public String getVehicle_year() {
		return vehicle_year;
	}

	public void setVehicle_primary_use(String value) {
		this.vehicle_primary_use = value;
	}

	public String getVehicle_primary_use() {
		return vehicle_primary_use;
	}

	public void setVehicle_kept_zip_code(String value) {
		this.vehicle_kept_zip_code = value;
	}

	public String getVehicle_kept_zip_code() {
		return vehicle_kept_zip_code;
	}

	public void setVehicle_ownership(String value) {
		this.vehicle_ownership = value;
	}

	public String getVehicle_ownership() {
		return vehicle_ownership;
	}

	public void setOwns(autoinsurance.Customer value) {
		this.owns = value;
	}

	public autoinsurance.Customer getOwns() {
		return owns;
	}

	public void setIs_created_for(autoinsurance.Insurance_Quote value) {
		this.is_created_for = value;
	}

	public autoinsurance.Insurance_Quote getIs_created_for() {
		return is_created_for;
	}

	public String toString() {
		return String.valueOf(getVehicle_VIN());
	}

	@Transient
	private boolean _saved = false;

	public void onSave() {
		_saved=true;
	}


	public void onLoad() {
		_saved=true;
	}


	public boolean isSaved() {
		return _saved;
	}




    boolean selected;
//    @Hidden
        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

    @ServiceMethod(/*mouseBinding = "left",*/ callByContent = true)
    public void select(){

        setSelected(!isSelected());
    }

}
