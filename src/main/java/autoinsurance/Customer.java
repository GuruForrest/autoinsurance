/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * <p/>
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 * <p/>
 * Modifying its content may cause the program not work, or your work may lost.
 * <p/>
 * Licensee: uengine
 * License Type: Purchased
 * <p/>
 * Licensee: uengine
 * License Type: Purchased
 */

/**
 * Licensee: uengine
 * License Type: Purchased
 */
package autoinsurance;

import org.metaworks.annotation.Range;
import org.metaworks.annotation.ServiceMethod;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@org.hibernate.annotations.Proxy(lazy = false)
@Table(name = "Customer")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "Discriminator", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("Customer")
public class Customer implements Serializable {
    @Column(name = "ID", nullable = false)
    @Id
    @GeneratedValue(generator = "AUTOINSURANCE_CUSTOMER_CUSTOMER_ID_GENERATOR")
    @org.hibernate.annotations.GenericGenerator(name = "AUTOINSURANCE_CUSTOMER_CUSTOMER_ID_GENERATOR", strategy = "native")
    private String customer_ID;
    @Column(name = "Customer_social_security_number", nullable = true, length = 255)
    private String customer_social_security_number;
    @Column(name = "Customer_first_name", nullable = true, length = 255)
    private String customer_first_name;
    @Column(name = "Customer_middle_initial", nullable = true, length = 255)
    private String customer_middle_initial;
    @Column(name = "Customer_last_name", nullable = true, length = 255)
    private String customer_last_name;
    @Column(name = "Customer_suffix", nullable = true, length = 255)
    private String customer_suffix;
    @Column(name = "Customer_mailing_address", nullable = true, length = 255)
    private String customer_mailing_address;
    @Column(name = "Customer_apt_unit_number", nullable = true, length = 255)
    private String customer_apt_unit_number;
    @Column(name = "Customer_city", nullable = true, length = 255)
    private String customer_city;
    @Column(name = "Customer_state", nullable = true, length = 255)
    private String customer_state;
    @Column(name = "Customer_zip_code", nullable = true, length = 255)
    private String customer_zip_code;
    @Column(name = "Customer_birth_date", nullable = true)
    @Temporal(TemporalType.DATE)
    private java.util.Date customer_birth_date;
    @Column(name = "Customer_gender", nullable = true, length = 255)
    private String customer_gender;
    @Column(name = "Customer_marital_status", nullable = true, length = 255)
//    @Range(options = {"married", "not married"}, values = {"married", "unmarried"})
    private String customer_marital_status;
    @Column(name = "Customer_driver_license_number", nullable = true, length = 255)
    private String customer_driver_license_number;
    @Column(name = "Customer_driver_license_status", nullable = true, length = 255)
    private String customer_driver_license_status;
    @Column(name = "Customer_credit_rating", nullable = true, length = 255)
    private String customer_credit_rating;
    @Column(name = "Customer_insurance_history_rating", nullable = true, length = 255)
    private String customer_insurance_history_rating;
    @OneToMany(mappedBy = "owns", targetEntity = autoinsurance.Vehicle.class)
    @org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})
    @org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)
    private java.util.Set vehicles = new java.util.HashSet();
    @OneToMany(mappedBy = "customer", targetEntity = autoinsurance.Insured_Driver.class)
    @org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})
    @org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)
    private java.util.Set insured_Drivers = new java.util.HashSet();
    @Transient
    private boolean _saved = false;
    public Customer() {
    }

    public String getCustomer_ID() {
        return customer_ID;
    }

    public void setCustomer_ID(String value) {
        this.customer_ID = value;
    }

    public String getORMID() {
        return getCustomer_ID();
    }

    public String getCustomer_social_security_number() {
        return customer_social_security_number;
    }

    public void setCustomer_social_security_number(String value) {
        this.customer_social_security_number = value;
    }

    public String getCustomer_first_name() {
        return customer_first_name;
    }

    public void setCustomer_first_name(String value) {
        this.customer_first_name = value;
    }

    public String getCustomer_middle_initial() {
        return customer_middle_initial;
    }

    public void setCustomer_middle_initial(String value) {
        this.customer_middle_initial = value;
    }

    public String getCustomer_last_name() {
        return customer_last_name;
    }

    public void setCustomer_last_name(String value) {
        this.customer_last_name = value;
    }

    @Range(options = {"Mr.", "Mrs.", "Miss."}, values = {"Mr.", "Mrs.", "Miss."})
    public String getCustomer_suffix() {
        return customer_suffix;
    }

    public void setCustomer_suffix(String value) {
        this.customer_suffix = value;
    }

    public String getCustomer_mailing_address() {
        return customer_mailing_address;
    }

    public void setCustomer_mailing_address(String value) {
        this.customer_mailing_address = value;
    }

    public String getCustomer_apt_unit_number() {
        return customer_apt_unit_number;
    }

    public void setCustomer_apt_unit_number(String value) {
        this.customer_apt_unit_number = value;
    }

    public String getCustomer_city() {
        return customer_city;
    }

    public void setCustomer_city(String value) {
        this.customer_city = value;
    }

    public String getCustomer_state() {
        return customer_state;
    }

    public void setCustomer_state(String value) {
        this.customer_state = value;
    }

    public String getCustomer_zip_code() {
        return customer_zip_code;
    }

    public void setCustomer_zip_code(String value) {
        this.customer_zip_code = value;
    }

    public java.util.Date getCustomer_birth_date() {
        return customer_birth_date;
    }

    public void setCustomer_birth_date(java.util.Date value) {
        this.customer_birth_date = value;
    }

    @Range(options = {"Male", "Female"}, values = {"m", "f"})
    public String getCustomer_gender() {
        return this.customer_gender;
    }

    public void setCustomer_gender(String value) {
        this.customer_gender = value;
    }

    @Range(options = {"미혼", "기혼", "돌싱"}, values = {"unmarried", "married", "divorced"})
    public String getCustomer_marital_status() {
        return customer_marital_status;
    }

    public void setCustomer_marital_status(String value) {
        this.customer_marital_status = value;
    }

    public String getCustomer_driver_license_number() {
        return customer_driver_license_number;
    }

    public void setCustomer_driver_license_number(String value) {
        this.customer_driver_license_number = value;
    }

    public String getCustomer_driver_license_status() {
        return customer_driver_license_status;
    }

    public void setCustomer_driver_license_status(String value) {
        this.customer_driver_license_status = value;
    }

    public String getCustomer_credit_rating() {
        return customer_credit_rating;
    }

    public void setCustomer_credit_rating(String value) {
        this.customer_credit_rating = value;
    }

    public String getCustomer_insurance_history_rating() {
        return customer_insurance_history_rating;
    }

    public void setCustomer_insurance_history_rating(String value) {
        this.customer_insurance_history_rating = value;
    }

    public java.util.Set getVehicles() {
        return vehicles;
    }

    public void setVehicles(java.util.Set value) {
        this.vehicles = value;
    }

    public autoinsurance.Insurance_Policy[] getInsurance_Policys() {
        java.util.ArrayList lValues = new java.util.ArrayList(5);
        for (java.util.Iterator lIter = insured_Drivers.iterator(); lIter.hasNext(); ) {
            lValues.add(((autoinsurance.Insured_Driver) lIter.next()).getInsurance_Policy());
        }
        return (autoinsurance.Insurance_Policy[]) lValues.toArray(new autoinsurance.Insurance_Policy[lValues.size()]);
    }

    public void removeInsurance_Policy(autoinsurance.Insurance_Policy aInsurance_Policy) {
        autoinsurance.Insured_Driver[] lInsured_Drivers = (autoinsurance.Insured_Driver[]) insured_Drivers.toArray(new autoinsurance.Insured_Driver[insured_Drivers.size()]);
        for (int i = 0; i < lInsured_Drivers.length; i++) {
            if (lInsured_Drivers[i].getInsurance_Policy().equals(aInsurance_Policy)) {
                insured_Drivers.remove(lInsured_Drivers[i]);
            }
        }
    }

    public void addInsurance_Policy(autoinsurance.Insured_Driver aInsured_Driver, autoinsurance.Insurance_Policy aInsurance_Policy) {
        aInsured_Driver.setInsurance_Policy(aInsurance_Policy);
        insured_Drivers.add(aInsured_Driver);
    }

    public autoinsurance.Insured_Driver getInsured_DriverByInsurance_Policy(autoinsurance.Insurance_Policy aInsurance_Policy) {
        autoinsurance.Insured_Driver[] lInsured_Drivers = (autoinsurance.Insured_Driver[]) insured_Drivers.toArray(new autoinsurance.Insured_Driver[insured_Drivers.size()]);
        for (int i = 0; i < lInsured_Drivers.length; i++) {
            if (lInsured_Drivers[i].getInsurance_Policy().equals(aInsurance_Policy)) {
                return lInsured_Drivers[i];
            }
        }
        return null;
    }

    public java.util.Set getInsured_Drivers() {
        return insured_Drivers;
    }

    public void setInsured_Drivers(java.util.Set value) {
        this.insured_Drivers = value;
    }

    public String toString() {
        return String.valueOf(getCustomer_ID());
    }

    public void onSave() {
        _saved = true;
    }


    public void onLoad() {
        _saved = true;
    }


    public boolean isSaved() {
        return _saved;
    }


    @ServiceMethod(callByContent = true)
    public void save() throws PersistentException {
        PersistentTransaction t = autoinsurance.AutoinsurancePersistentManager.instance().getSession().beginTransaction();

        try {

//            autoinsurance.Customer customer = new autoinsurance.Customer();
//
//            customer.setCustomer_ID(getCustomer_ID());
//            customer.setCustomer_first_name(getCustomer_first_name());
//

            autoinsurance.AutoinsurancePersistentManager.instance().getSession().save(this);

            t.commit();
        } catch (Exception e) {

            t.rollback();

            throw e;
        }


    }

}
