/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * <p/>
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 * <p/>
 * Modifying its content may cause the program not work, or your work may lost.
 * <p/>
 * Licensee: uengine
 * License Type: Purchased
 * <p/>
 * Licensee: uengine
 * License Type: Purchased
 * <p/>
 * Licensee: uengine
 * License Type: Purchased
 * <p/>
 * Licensee: uengine
 * License Type: Purchased
 * <p/>
 * Licensee: uengine
 * License Type: Purchased
 * <p/>
 * Licensee: uengine
 * License Type: Purchased
 * <p/>
 * Licensee: uengine
 * License Type: Purchased
 */

/**
 * Licensee: uengine
 * License Type: Purchased
 */
package autoinsurance;

import org.metaworks.ContextAware;
import org.metaworks.MetaworksContext;
import org.metaworks.annotation.Range;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("Policyholder")
//@Face(ejsPathMappingByContext = {
//        "{where: 'NameAndAddressTab', face: 'dwr/metaworks/autoinsurance/Policyholder_NameAndAdress.ejs'}",
//        "{where: 'DriversTab', face: 'dwr/metaworks/autoinsurance/Policyholder_Drivers.ejs'}",
//        "{where: 'FinalDetailsTab', face: 'dwr/metaworks/autoinsurance/Policyholder_FinalDetails.ejs'}"
//})
public class Policyholder extends autoinsurance.Customer implements Serializable, ContextAware {
    private MetaworksContext metaworksContext;
    @Column(name = "Customer_email_address", nullable = true, length = 255)
    private String customer_email_address;
    @Column(name = "Customer_primary_residence", nullable = true, length = 255)
    private String customer_primary_residence;
    @Column(name = "Customer_highest_education_level", nullable = true, length = 255)
    private String customer_highest_education_level;
    @Column(name = "Customer_move_history", nullable = true, length = 255)
    private String customer_move_history;
    @Column(name = "Customer_accident_history", nullable = true, length = 255)
    private String customer_accident_history;
    @Column(name = "Customer_health_insurance", nullable = true, length = 255)
    private String customer_health_insurance;
    @Column(name = "Customer_Progressive_insurance", nullable = true, length = 255)
    private String customer_Progressive_insurance;

    public Policyholder() {
    }

    @Override
    public MetaworksContext getMetaworksContext() {
        return metaworksContext;
    }

    @Override
    public void setMetaworksContext(MetaworksContext metaworksContext) {
        this.metaworksContext = metaworksContext;
    }

    public String getCustomer_email_address() {
        return customer_email_address;
    }

    public void setCustomer_email_address(String value) {
        this.customer_email_address = value;
    }

    public String getCustomer_primary_residence() {
        return customer_primary_residence;
    }

    public void setCustomer_primary_residence(String value) {
        this.customer_primary_residence = value;
    }

    @Range(options = {"대학원", "대학교", "대학", "고등학교"}, values = {"phd", "uni", "coll", "high"})
    public String getCustomer_highest_education_level() {
        return customer_highest_education_level;
    }

    public void setCustomer_highest_education_level(String value) {
        this.customer_highest_education_level = value;
    }

    @Range(options = {"Yes", "No"}, values = {"y", "n"})
    public String getCustomer_move_history() {
        return customer_move_history;
    }


    public void setCustomer_move_history(String value) {
        this.customer_move_history = value;
    }

    @Range(options = {"Yes", "No"}, values = {"y", "n"})
    public String getCustomer_accident_history() {
        return customer_accident_history;
    }

    public void setCustomer_accident_history(String value) {
        this.customer_accident_history = value;
    }

    public String getCustomer_health_insurance() {
        return customer_health_insurance;
    }

    public void setCustomer_health_insurance(String value) {
        this.customer_health_insurance = value;
    }

    public String getCustomer_Progressive_insurance() {
        return customer_Progressive_insurance;
    }

    public void setCustomer_Progressive_insurance(String value) {
        this.customer_Progressive_insurance = value;
    }

    public String toString() {
        return super.toString();
    }

}
