/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: uengine
 * License Type: Purchased
 */
package autoinsurance;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Insurance_Policy_Payment")
public class Insurance_Policy_Payment implements Serializable {
	public Insurance_Policy_Payment() {
	}
	
	@Column(name="ID", nullable=false)	
	@Id	
	@GeneratedValue(generator="AUTOINSURANCE_INSURANCE_POLICY_PAYMENT_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="AUTOINSURANCE_INSURANCE_POLICY_PAYMENT_ID_GENERATOR", strategy="native")	
	private int ID;
	
	@OneToOne(targetEntity=autoinsurance.Insurance_Policy.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="Insurance_PolicyID", nullable=false) })	
	private autoinsurance.Insurance_Policy insurance_Policy;
	
	@Column(name="Insurance_policy_payment_date", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date insurance_policy_payment_date;
	
	@Column(name="Insurance_policy_payment_amount", nullable=true)	
	private double insurance_policy_payment_amount;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setInsurance_policy_payment_date(java.util.Date value) {
		this.insurance_policy_payment_date = value;
	}
	
	public java.util.Date getInsurance_policy_payment_date() {
		return insurance_policy_payment_date;
	}
	
	public void setInsurance_policy_payment_amount(double value) {
		this.insurance_policy_payment_amount = value;
	}
	
	public double getInsurance_policy_payment_amount() {
		return insurance_policy_payment_amount;
	}
	
	public void setInsurance_Policy(autoinsurance.Insurance_Policy value) {
		this.insurance_Policy = value;
	}
	
	public autoinsurance.Insurance_Policy getInsurance_Policy() {
		return insurance_Policy;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
