/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: uengine
 * License Type: Purchased
 */
package autoinsurance;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Insured_Driver")
public class Insured_Driver implements Serializable {
	public Insured_Driver() {
	}
	
	@Column(name="ID", nullable=false)	
	@Id	
	@GeneratedValue(generator="AUTOINSURANCE_INSURED_DRIVER_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="AUTOINSURANCE_INSURED_DRIVER_ID_GENERATOR", strategy="native")	
	private int ID;
	
	@Column(name="Insured_driver_relationship_to_policyholder", nullable=true, length=255)	
	private String insured_driver_relationship_to_policyholder;
	
	@ManyToOne(targetEntity=autoinsurance.Insurance_Policy.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="Insurance_PolicyID", referencedColumnName="ID", nullable=false) })	
	private autoinsurance.Insurance_Policy insurance_Policy;
	
	@ManyToOne(targetEntity=autoinsurance.Customer.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="CustomerID", referencedColumnName="ID", nullable=false) })	
	private autoinsurance.Customer customer;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setInsured_driver_relationship_to_policyholder(String value) {
		this.insured_driver_relationship_to_policyholder = value;
	}
	
	public String getInsured_driver_relationship_to_policyholder() {
		return insured_driver_relationship_to_policyholder;
	}
	
	public void setCustomer(autoinsurance.Customer value) {
		this.customer = value;
	}
	
	public autoinsurance.Customer getCustomer() {
		return customer;
	}
	
	public void setInsurance_Policy(autoinsurance.Insurance_Policy value) {
		this.insurance_Policy = value;
	}
	
	public autoinsurance.Insurance_Policy getInsurance_Policy() {
		return insurance_Policy;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
