/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: uengine
 * License Type: Purchased
 */
package autoinsurance;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Insurance_Product")
public class Insurance_Product implements Serializable {
	public Insurance_Product() {
	}
	
	@Column(name="ID", nullable=false)	
	@Id	
	@GeneratedValue(generator="AUTOINSURANCE_INSURANCE_PRODUCT_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="AUTOINSURANCE_INSURANCE_PRODUCT_ID_GENERATOR", strategy="native")	
	private int ID;
	
	@OneToOne(targetEntity=autoinsurance.Coverage_Item.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="Coverage_ItemID", nullable=false) })	
	private autoinsurance.Coverage_Item coverage_Item;
	
	@Column(name="Insurance_product_code", nullable=true, length=255)	
	private String insurance_product_code;
	
	@Column(name="Insurance_product_name", nullable=true, length=255)	
	private String insurance_product_name;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setInsurance_product_code(String value) {
		this.insurance_product_code = value;
	}
	
	public String getInsurance_product_code() {
		return insurance_product_code;
	}
	
	public void setInsurance_product_name(String value) {
		this.insurance_product_name = value;
	}
	
	public String getInsurance_product_name() {
		return insurance_product_name;
	}
	
	public void setCoverage_Item(autoinsurance.Coverage_Item value) {
		this.coverage_Item = value;
	}
	
	public autoinsurance.Coverage_Item getCoverage_Item() {
		return coverage_Item;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}

	
}
