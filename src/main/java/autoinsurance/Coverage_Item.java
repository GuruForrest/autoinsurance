/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: uengine
 * License Type: Purchased
 */
package autoinsurance;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Coverage_Item")
public class Coverage_Item implements Serializable {
	public Coverage_Item() {
	}
	
	@Column(name="ID", nullable=false)	
	@Id	
	@GeneratedValue(generator="AUTOINSURANCE_COVERAGE_ITEM_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="AUTOINSURANCE_COVERAGE_ITEM_ID_GENERATOR", strategy="native")	
	private int ID;
	
	@OneToOne(targetEntity=autoinsurance.Coverage_Item_Option.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="Coverage_Item_OptionID", nullable=false) })	
	private autoinsurance.Coverage_Item_Option coverage_Item_Option;
	
	@Column(name="Coverage_item_code", nullable=true, length=255)	
	private String coverage_item_code;
	
	@Column(name="Coverage_item_name", nullable=true, length=255)	
	private String coverage_item_name;
	
	@OneToOne(mappedBy="coverage_Item", targetEntity=autoinsurance.Insurance_Product.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	private autoinsurance.Insurance_Product insurance_Product;
	
	@OneToMany(mappedBy="coverage_Item", targetEntity=autoinsurance.Coverage_Item_Option_Selected.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set coverage_Item_Option_Selecteds = new java.util.HashSet();
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setCoverage_item_code(String value) {
		this.coverage_item_code = value;
	}
	
	public String getCoverage_item_code() {
		return coverage_item_code;
	}
	
	public void setCoverage_item_name(String value) {
		this.coverage_item_name = value;
	}
	
	public String getCoverage_item_name() {
		return coverage_item_name;
	}
	
	public void setCoverage_Item_Option(autoinsurance.Coverage_Item_Option value) {
		this.coverage_Item_Option = value;
	}
	
	public autoinsurance.Coverage_Item_Option getCoverage_Item_Option() {
		return coverage_Item_Option;
	}
	
	public void setInsurance_Product(autoinsurance.Insurance_Product value) {
		this.insurance_Product = value;
	}
	
	public autoinsurance.Insurance_Product getInsurance_Product() {
		return insurance_Product;
	}
	
	public autoinsurance.Insurance_Policy[] getInsurance_Policys() {
		java.util.ArrayList lValues = new java.util.ArrayList(5);
		for(java.util.Iterator lIter = coverage_Item_Option_Selecteds.iterator();lIter.hasNext();) {
			lValues.add(((autoinsurance.Coverage_Item_Option_Selected)lIter.next()).getInsurance_Policy());
		}
		return (autoinsurance.Insurance_Policy[])lValues.toArray(new autoinsurance.Insurance_Policy[lValues.size()]);
	}
	
	public void removeInsurance_Policy(autoinsurance.Insurance_Policy aInsurance_Policy) {
		autoinsurance.Coverage_Item_Option_Selected[] lCoverage_Item_Option_Selecteds = (autoinsurance.Coverage_Item_Option_Selected[])coverage_Item_Option_Selecteds.toArray(new autoinsurance.Coverage_Item_Option_Selected[coverage_Item_Option_Selecteds.size()]);
		for(int i = 0; i < lCoverage_Item_Option_Selecteds.length; i++) {
			if(lCoverage_Item_Option_Selecteds[i].getInsurance_Policy().equals(aInsurance_Policy)) {
				coverage_Item_Option_Selecteds.remove(lCoverage_Item_Option_Selecteds[i]);
			}
		}
	}
	
	public void addInsurance_Policy(autoinsurance.Coverage_Item_Option_Selected aCoverage_Item_Option_Selected, autoinsurance.Insurance_Policy aInsurance_Policy) {
		aCoverage_Item_Option_Selected.setInsurance_Policy(aInsurance_Policy);
		coverage_Item_Option_Selecteds.add(aCoverage_Item_Option_Selected);
	}
	
	public autoinsurance.Coverage_Item_Option_Selected getCoverage_Item_Option_SelectedByInsurance_Policy(autoinsurance.Insurance_Policy aInsurance_Policy) {
		autoinsurance.Coverage_Item_Option_Selected[] lCoverage_Item_Option_Selecteds = (autoinsurance.Coverage_Item_Option_Selected[])coverage_Item_Option_Selecteds.toArray(new autoinsurance.Coverage_Item_Option_Selected[coverage_Item_Option_Selecteds.size()]);
		for(int i = 0; i < lCoverage_Item_Option_Selecteds.length; i++) {
			if(lCoverage_Item_Option_Selecteds[i].getInsurance_Policy().equals(aInsurance_Policy)) {
				return lCoverage_Item_Option_Selecteds[i];
			}
		}
		return null;
	}
	
	public void setCoverage_Item_Option_Selecteds(java.util.Set value) {
		this.coverage_Item_Option_Selecteds = value;
	}
	
	public java.util.Set getCoverage_Item_Option_Selecteds() {
		return coverage_Item_Option_Selecteds;
	}
	
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
