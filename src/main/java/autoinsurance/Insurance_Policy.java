/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: uengine
 * License Type: Purchased
 */
package autoinsurance;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Insurance_Policy")
public class Insurance_Policy implements Serializable {
	public Insurance_Policy() {
	}
	
	@Column(name="ID", nullable=false)	
	@Id	
	@GeneratedValue(generator="AUTOINSURANCE_INSURANCE_POLICY_POLICY_NUMBER_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="AUTOINSURANCE_INSURANCE_POLICY_POLICY_NUMBER_GENERATOR", strategy="native")	
	private String policy_number;
	
	@OneToOne(targetEntity=autoinsurance.Insurance_Quote.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="Insurance_QuoteID", nullable=false) })	
	private autoinsurance.Insurance_Quote insurance_Quote;
	
	@Column(name="Policy_coverage_start_date", nullable=true, length=255)	
	private String policy_coverage_start_date;
	
	@Column(name="Policy_bill_plan", nullable=true, length=255)	
	private String policy_bill_plan;
	
	@Column(name="Policy_signature", nullable=true, length=255)	
	private String policy_signature;
	
	@Column(name="Insurance_premium", nullable=true, length=255)	
	private String insurance_premium;
	
	@OneToMany(mappedBy="insurance_Policy", targetEntity=autoinsurance.Coverage_Item_Option_Selected.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set coverage_Item_Option_Selecteds = new java.util.HashSet();
	
	@OneToMany(mappedBy="insurance_Policy", targetEntity=autoinsurance.Insured_Driver.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set insured_Drivers = new java.util.HashSet();
	
	@OneToOne(mappedBy="insurance_Policy", targetEntity=autoinsurance.Insurance_Policy_Payment.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	private autoinsurance.Insurance_Policy_Payment insurance_Policy_Payment;
	
	public void setPolicy_number(String value) {
		this.policy_number = value;
	}
	
	public String getPolicy_number() {
		return policy_number;
	}
	
	public String getORMID() {
		return getPolicy_number();
	}
	
	public void setPolicy_coverage_start_date(String value) {
		this.policy_coverage_start_date = value;
	}
	
	public String getPolicy_coverage_start_date() {
		return policy_coverage_start_date;
	}
	
	public void setPolicy_bill_plan(String value) {
		this.policy_bill_plan = value;
	}
	
	public String getPolicy_bill_plan() {
		return policy_bill_plan;
	}
	
	public void setPolicy_signature(String value) {
		this.policy_signature = value;
	}
	
	public String getPolicy_signature() {
		return policy_signature;
	}
	
	public void setInsurance_premium(String value) {
		this.insurance_premium = value;
	}
	
	public String getInsurance_premium() {
		return insurance_premium;
	}
	
	public void setInsurance_Quote(autoinsurance.Insurance_Quote value) {
		this.insurance_Quote = value;
	}
	
	public autoinsurance.Insurance_Quote getInsurance_Quote() {
		return insurance_Quote;
	}
	
	public autoinsurance.Coverage_Item[] getCoverage_Items() {
		java.util.ArrayList lValues = new java.util.ArrayList(5);
		for(java.util.Iterator lIter = coverage_Item_Option_Selecteds.iterator();lIter.hasNext();) {
			lValues.add(((autoinsurance.Coverage_Item_Option_Selected)lIter.next()).getCoverage_Item());
		}
		return (autoinsurance.Coverage_Item[])lValues.toArray(new autoinsurance.Coverage_Item[lValues.size()]);
	}
	
	public void removeCoverage_Item(autoinsurance.Coverage_Item aCoverage_Item) {
		autoinsurance.Coverage_Item_Option_Selected[] lCoverage_Item_Option_Selecteds = (autoinsurance.Coverage_Item_Option_Selected[])coverage_Item_Option_Selecteds.toArray(new autoinsurance.Coverage_Item_Option_Selected[coverage_Item_Option_Selecteds.size()]);
		for(int i = 0; i < lCoverage_Item_Option_Selecteds.length; i++) {
			if(lCoverage_Item_Option_Selecteds[i].getCoverage_Item().equals(aCoverage_Item)) {
				coverage_Item_Option_Selecteds.remove(lCoverage_Item_Option_Selecteds[i]);
			}
		}
	}
	
	public void addCoverage_Item(autoinsurance.Coverage_Item_Option_Selected aCoverage_Item_Option_Selected, autoinsurance.Coverage_Item aCoverage_Item) {
		aCoverage_Item_Option_Selected.setCoverage_Item(aCoverage_Item);
		coverage_Item_Option_Selecteds.add(aCoverage_Item_Option_Selected);
	}
	
	public autoinsurance.Coverage_Item_Option_Selected getCoverage_Item_Option_SelectedByCoverage_Item(autoinsurance.Coverage_Item aCoverage_Item) {
		autoinsurance.Coverage_Item_Option_Selected[] lCoverage_Item_Option_Selecteds = (autoinsurance.Coverage_Item_Option_Selected[])coverage_Item_Option_Selecteds.toArray(new autoinsurance.Coverage_Item_Option_Selected[coverage_Item_Option_Selecteds.size()]);
		for(int i = 0; i < lCoverage_Item_Option_Selecteds.length; i++) {
			if(lCoverage_Item_Option_Selecteds[i].getCoverage_Item().equals(aCoverage_Item)) {
				return lCoverage_Item_Option_Selecteds[i];
			}
		}
		return null;
	}
	
	public void setCoverage_Item_Option_Selecteds(java.util.Set value) {
		this.coverage_Item_Option_Selecteds = value;
	}
	
	public java.util.Set getCoverage_Item_Option_Selecteds() {
		return coverage_Item_Option_Selecteds;
	}
	
	
	public autoinsurance.Customer[] getCustomers() {
		java.util.ArrayList lValues = new java.util.ArrayList(5);
		for(java.util.Iterator lIter = insured_Drivers.iterator();lIter.hasNext();) {
			lValues.add(((autoinsurance.Insured_Driver)lIter.next()).getCustomer());
		}
		return (autoinsurance.Customer[])lValues.toArray(new autoinsurance.Customer[lValues.size()]);
	}
	
	public void removeCustomer(autoinsurance.Customer aCustomer) {
		autoinsurance.Insured_Driver[] lInsured_Drivers = (autoinsurance.Insured_Driver[])insured_Drivers.toArray(new autoinsurance.Insured_Driver[insured_Drivers.size()]);
		for(int i = 0; i < lInsured_Drivers.length; i++) {
			if(lInsured_Drivers[i].getCustomer().equals(aCustomer)) {
				insured_Drivers.remove(lInsured_Drivers[i]);
			}
		}
	}
	
	public void addCustomer(autoinsurance.Insured_Driver aInsured_Driver, autoinsurance.Customer aCustomer) {
		aInsured_Driver.setCustomer(aCustomer);
		insured_Drivers.add(aInsured_Driver);
	}
	
	public autoinsurance.Insured_Driver getInsured_DriverByCustomer(autoinsurance.Customer aCustomer) {
		autoinsurance.Insured_Driver[] lInsured_Drivers = (autoinsurance.Insured_Driver[])insured_Drivers.toArray(new autoinsurance.Insured_Driver[insured_Drivers.size()]);
		for(int i = 0; i < lInsured_Drivers.length; i++) {
			if(lInsured_Drivers[i].getCustomer().equals(aCustomer)) {
				return lInsured_Drivers[i];
			}
		}
		return null;
	}
	
	public void setInsured_Drivers(java.util.Set value) {
		this.insured_Drivers = value;
	}
	
	public java.util.Set getInsured_Drivers() {
		return insured_Drivers;
	}
	
	
	public void setInsurance_Policy_Payment(autoinsurance.Insurance_Policy_Payment value) {
		this.insurance_Policy_Payment = value;
	}
	
	public autoinsurance.Insurance_Policy_Payment getInsurance_Policy_Payment() {
		return insurance_Policy_Payment;
	}
	
	public String toString() {
		return String.valueOf(getPolicy_number());
	}
	
	@Transient	
	private boolean _saved = false;
	
	public void onSave() {
		_saved=true;
	}
	
	
	public void onLoad() {
		_saved=true;
	}
	
	
	public boolean isSaved() {
		return _saved;
	}



    public void createInsurancePolicy() throws PersistentException {

        PersistentTransaction t = autoinsurance.AutoinsurancePersistentManager.instance().getSession().beginTransaction();

        try{

            autoinsurance.AutoinsurancePersistentManager.instance().getSession().save(this);

            t.commit();
        }
        catch (Exception e) {

            t.rollback();

            throw e;
        }

    }
	
	
}
