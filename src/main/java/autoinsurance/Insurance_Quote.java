/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: uengine
 * License Type: Purchased
 */
package autoinsurance;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Insurance_Quote")
public class Insurance_Quote implements Serializable {
	public Insurance_Quote() {
	}
	
	@Column(name="ID", nullable=false)	
	@Id	
	@GeneratedValue(generator="AUTOINSURANCE_INSURANCE_QUOTE_QUOTE_NUMBER_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="AUTOINSURANCE_INSURANCE_QUOTE_QUOTE_NUMBER_GENERATOR", strategy="native")	
	private String quote_number;
	
	@OneToOne(targetEntity=autoinsurance.Vehicle.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="VehicleID", nullable=false) })	
	private autoinsurance.Vehicle vehicle;
	
	@Column(name="Quote_saved_time_stamp", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date quote_saved_time_stamp;
	
	@Column(name="Most_recent_insurer", nullable=true, length=255)	
	private String most_recent_insurer;
	
	@Column(name="Most_recent_policy_number", nullable=true, length=255)	
	private String most_recent_policy_number;
	
	@OneToOne(mappedBy="insurance_Quote", targetEntity=autoinsurance.Insurance_Policy.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	private autoinsurance.Insurance_Policy is_issued_for;
	
	public void setQuote_number(String value) {
		this.quote_number = value;
	}
	
	public String getQuote_number() {
		return quote_number;
	}
	
	public String getORMID() {
		return getQuote_number();
	}
	
	public void setQuote_saved_time_stamp(java.util.Date value) {
		this.quote_saved_time_stamp = value;
	}
	
	public java.util.Date getQuote_saved_time_stamp() {
		return quote_saved_time_stamp;
	}
	
	public void setMost_recent_insurer(String value) {
		this.most_recent_insurer = value;
	}
	
	public String getMost_recent_insurer() {
		return most_recent_insurer;
	}
	
	public void setMost_recent_policy_number(String value) {
		this.most_recent_policy_number = value;
	}
	
	public String getMost_recent_policy_number() {
		return most_recent_policy_number;
	}
	
	public void setVehicle(autoinsurance.Vehicle value) {
		this.vehicle = value;
	}
	
	public autoinsurance.Vehicle getVehicle() {
		return vehicle;
	}
	
	public void setIs_issued_for(autoinsurance.Insurance_Policy value) {
		this.is_issued_for = value;
	}
	
	public autoinsurance.Insurance_Policy getIs_issued_for() {
		return is_issued_for;
	}
	
	public String toString() {
		return String.valueOf(getQuote_number());
	}
	
	@Transient	
	private boolean _saved = false;
	
	public void onSave() {
		_saved=true;
	}
	
	
	public void onLoad() {
		_saved=true;
	}
	
	
	public boolean isSaved() {
		return _saved;
	}
	
	
}
