/**
 * Licensee: uengine
 * License Type: Purchased
 */
package ormsamples;

import org.orm.*;
public class RetrieveAndUpdateAutoinsuranceData {
	public void retrieveAndUpdateTestData() throws PersistentException {
		PersistentTransaction t = autoinsurance.AutoinsurancePersistentManager.instance().getSession().beginTransaction();
		try {
			autoinsurance.Coverage_Item lautoinsuranceCoverage_Item= (autoinsurance.Coverage_Item)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Coverage_Item").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().update(lautoinsuranceCoverage_Item);
			
			autoinsurance.Coverage_Item_Option lautoinsuranceCoverage_Item_Option= (autoinsurance.Coverage_Item_Option)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Coverage_Item_Option").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().update(lautoinsuranceCoverage_Item_Option);
			
			autoinsurance.Coverage_Item_Option_Selected lautoinsuranceCoverage_Item_Option_Selected= (autoinsurance.Coverage_Item_Option_Selected)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Coverage_Item_Option_Selected").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().update(lautoinsuranceCoverage_Item_Option_Selected);
			
			autoinsurance.Customer lautoinsuranceCustomer= (autoinsurance.Customer)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Customer").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().update(lautoinsuranceCustomer);
			
			autoinsurance.Insurance_Policy lautoinsuranceInsurance_Policy= (autoinsurance.Insurance_Policy)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Insurance_Policy").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().update(lautoinsuranceInsurance_Policy);
			
			autoinsurance.Insurance_Policy_Payment lautoinsuranceInsurance_Policy_Payment= (autoinsurance.Insurance_Policy_Payment)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Insurance_Policy_Payment").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().update(lautoinsuranceInsurance_Policy_Payment);
			
			autoinsurance.Insurance_Product lautoinsuranceInsurance_Product= (autoinsurance.Insurance_Product)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Insurance_Product").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().update(lautoinsuranceInsurance_Product);
			
			autoinsurance.Insurance_Quote lautoinsuranceInsurance_Quote= (autoinsurance.Insurance_Quote)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Insurance_Quote").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().update(lautoinsuranceInsurance_Quote);
			
			autoinsurance.Insured_Driver lautoinsuranceInsured_Driver= (autoinsurance.Insured_Driver)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Insured_Driver").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().update(lautoinsuranceInsured_Driver);
			
			autoinsurance.Policyholder lautoinsurancePolicyholder= (autoinsurance.Policyholder)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Policyholder").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().update(lautoinsurancePolicyholder);
			
			autoinsurance.Vehicle lautoinsuranceVehicle= (autoinsurance.Vehicle)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Vehicle").setMaxResults(1).uniqueResult();
			// Update the properties of the persistent object
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().update(lautoinsuranceVehicle);
			
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}
	
	public static void main(String[] args) {
		try {
			RetrieveAndUpdateAutoinsuranceData retrieveAndUpdateAutoinsuranceData = new RetrieveAndUpdateAutoinsuranceData();
			try {
				retrieveAndUpdateAutoinsuranceData.retrieveAndUpdateTestData();
			}
			finally {
				autoinsurance.AutoinsurancePersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
