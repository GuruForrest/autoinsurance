/**
 * Licensee: uengine
 * License Type: Purchased
 */
package ormsamples;

import org.orm.*;
public class CreateAutoinsuranceData {
	public void createTestData() throws PersistentException {
		PersistentTransaction t = autoinsurance.AutoinsurancePersistentManager.instance().getSession().beginTransaction();
		try {
			autoinsurance.Coverage_Item lautoinsuranceCoverage_Item = new autoinsurance.Coverage_Item();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : coverage_Item_Option_Selecteds, insurance_Product, coverage_Item_Option
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().save(lautoinsuranceCoverage_Item);
			
			autoinsurance.Coverage_Item_Option lautoinsuranceCoverage_Item_Option = new autoinsurance.Coverage_Item_Option();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : coverage_Item
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().save(lautoinsuranceCoverage_Item_Option);
			
			autoinsurance.Coverage_Item_Option_Selected lautoinsuranceCoverage_Item_Option_Selected = new autoinsurance.Coverage_Item_Option_Selected();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : coverage_Item, insurance_Policy
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().save(lautoinsuranceCoverage_Item_Option_Selected);
			
			autoinsurance.Customer lautoinsuranceCustomer = new autoinsurance.Customer();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : insured_Drivers, vehicles
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().save(lautoinsuranceCustomer);
			
			autoinsurance.Insurance_Policy lautoinsuranceInsurance_Policy = new autoinsurance.Insurance_Policy();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : insurance_Policy_Payment, insured_Drivers, coverage_Item_Option_Selecteds, insurance_Quote
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().save(lautoinsuranceInsurance_Policy);
			
			autoinsurance.Insurance_Policy_Payment lautoinsuranceInsurance_Policy_Payment = new autoinsurance.Insurance_Policy_Payment();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : insurance_Policy
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().save(lautoinsuranceInsurance_Policy_Payment);
			
			autoinsurance.Insurance_Product lautoinsuranceInsurance_Product = new autoinsurance.Insurance_Product();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : coverage_Item
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().save(lautoinsuranceInsurance_Product);
			
			autoinsurance.Insurance_Quote lautoinsuranceInsurance_Quote = new autoinsurance.Insurance_Quote();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : is_issued_for, vehicle
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().save(lautoinsuranceInsurance_Quote);
			
			autoinsurance.Insured_Driver lautoinsuranceInsured_Driver = new autoinsurance.Insured_Driver();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : customer, insurance_Policy
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().save(lautoinsuranceInsured_Driver);
			
			autoinsurance.Policyholder lautoinsurancePolicyholder = new autoinsurance.Policyholder();			// Initialize the properties of the persistent object here
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().save(lautoinsurancePolicyholder);
			
			autoinsurance.Vehicle lautoinsuranceVehicle = new autoinsurance.Vehicle();			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : is_created_for, owns
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().save(lautoinsuranceVehicle);
			
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			CreateAutoinsuranceData createAutoinsuranceData = new CreateAutoinsuranceData();
			try {
				createAutoinsuranceData.createTestData();
			}
			finally {
				autoinsurance.AutoinsurancePersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
