/**
 * Licensee: uengine
 * License Type: Purchased
 */
package ormsamples;

import org.orm.*;
public class DeleteAutoinsuranceData {
	public void deleteTestData() throws PersistentException {
		PersistentTransaction t = autoinsurance.AutoinsurancePersistentManager.instance().getSession().beginTransaction();
		try {
			autoinsurance.Coverage_Item lautoinsuranceCoverage_Item= (autoinsurance.Coverage_Item)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Coverage_Item").setMaxResults(1).uniqueResult();
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().delete(lautoinsuranceCoverage_Item);
			
			autoinsurance.Coverage_Item_Option lautoinsuranceCoverage_Item_Option= (autoinsurance.Coverage_Item_Option)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Coverage_Item_Option").setMaxResults(1).uniqueResult();
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().delete(lautoinsuranceCoverage_Item_Option);
			
			autoinsurance.Coverage_Item_Option_Selected lautoinsuranceCoverage_Item_Option_Selected= (autoinsurance.Coverage_Item_Option_Selected)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Coverage_Item_Option_Selected").setMaxResults(1).uniqueResult();
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().delete(lautoinsuranceCoverage_Item_Option_Selected);
			
			autoinsurance.Customer lautoinsuranceCustomer= (autoinsurance.Customer)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Customer").setMaxResults(1).uniqueResult();
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().delete(lautoinsuranceCustomer);
			
			autoinsurance.Insurance_Policy lautoinsuranceInsurance_Policy= (autoinsurance.Insurance_Policy)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Insurance_Policy").setMaxResults(1).uniqueResult();
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().delete(lautoinsuranceInsurance_Policy);
			
			autoinsurance.Insurance_Policy_Payment lautoinsuranceInsurance_Policy_Payment= (autoinsurance.Insurance_Policy_Payment)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Insurance_Policy_Payment").setMaxResults(1).uniqueResult();
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().delete(lautoinsuranceInsurance_Policy_Payment);
			
			autoinsurance.Insurance_Product lautoinsuranceInsurance_Product= (autoinsurance.Insurance_Product)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Insurance_Product").setMaxResults(1).uniqueResult();
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().delete(lautoinsuranceInsurance_Product);
			
			autoinsurance.Insurance_Quote lautoinsuranceInsurance_Quote= (autoinsurance.Insurance_Quote)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Insurance_Quote").setMaxResults(1).uniqueResult();
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().delete(lautoinsuranceInsurance_Quote);
			
			autoinsurance.Insured_Driver lautoinsuranceInsured_Driver= (autoinsurance.Insured_Driver)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Insured_Driver").setMaxResults(1).uniqueResult();
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().delete(lautoinsuranceInsured_Driver);
			
			autoinsurance.Policyholder lautoinsurancePolicyholder= (autoinsurance.Policyholder)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Policyholder").setMaxResults(1).uniqueResult();
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().delete(lautoinsurancePolicyholder);
			
			autoinsurance.Vehicle lautoinsuranceVehicle= (autoinsurance.Vehicle)autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Vehicle").setMaxResults(1).uniqueResult();
			autoinsurance.AutoinsurancePersistentManager.instance().getSession().delete(lautoinsuranceVehicle);
			
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}
	
	public static void main(String[] args) {
		try {
			DeleteAutoinsuranceData deleteAutoinsuranceData = new DeleteAutoinsuranceData();
			try {
				deleteAutoinsuranceData.deleteTestData();
			}
			finally {
				autoinsurance.AutoinsurancePersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
