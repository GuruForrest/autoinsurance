/**
 * Licensee: uengine
 * License Type: Purchased
 */
package ormsamples;

import org.orm.*;
public class ListAutoinsuranceData {
	private static final int ROW_COUNT = 100;
	
	public void listTestData() throws PersistentException {
		System.out.println("Listing Coverage_Item...");
		java.util.List lCoverage_ItemList = autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Coverage_Item").setMaxResults(ROW_COUNT).list();
		autoinsurance.Coverage_Item[] lautoinsuranceCoverage_Items = (autoinsurance.Coverage_Item[]) lCoverage_ItemList.toArray(new autoinsurance.Coverage_Item[lCoverage_ItemList.size()]);
		int length = Math.min(lautoinsuranceCoverage_Items.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lautoinsuranceCoverage_Items[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Coverage_Item_Option...");
		java.util.List lCoverage_Item_OptionList = autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Coverage_Item_Option").setMaxResults(ROW_COUNT).list();
		autoinsurance.Coverage_Item_Option[] lautoinsuranceCoverage_Item_Options = (autoinsurance.Coverage_Item_Option[]) lCoverage_Item_OptionList.toArray(new autoinsurance.Coverage_Item_Option[lCoverage_Item_OptionList.size()]);
		length = Math.min(lautoinsuranceCoverage_Item_Options.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lautoinsuranceCoverage_Item_Options[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Coverage_Item_Option_Selected...");
		java.util.List lCoverage_Item_Option_SelectedList = autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Coverage_Item_Option_Selected").setMaxResults(ROW_COUNT).list();
		autoinsurance.Coverage_Item_Option_Selected[] lautoinsuranceCoverage_Item_Option_Selecteds = (autoinsurance.Coverage_Item_Option_Selected[]) lCoverage_Item_Option_SelectedList.toArray(new autoinsurance.Coverage_Item_Option_Selected[lCoverage_Item_Option_SelectedList.size()]);
		length = Math.min(lautoinsuranceCoverage_Item_Option_Selecteds.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lautoinsuranceCoverage_Item_Option_Selecteds[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Customer...");
		java.util.List lCustomerList = autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Customer").setMaxResults(ROW_COUNT).list();
		autoinsurance.Customer[] lautoinsuranceCustomers = (autoinsurance.Customer[]) lCustomerList.toArray(new autoinsurance.Customer[lCustomerList.size()]);
		length = Math.min(lautoinsuranceCustomers.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lautoinsuranceCustomers[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Insurance_Policy...");
		java.util.List lInsurance_PolicyList = autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Insurance_Policy").setMaxResults(ROW_COUNT).list();
		autoinsurance.Insurance_Policy[] lautoinsuranceInsurance_Policys = (autoinsurance.Insurance_Policy[]) lInsurance_PolicyList.toArray(new autoinsurance.Insurance_Policy[lInsurance_PolicyList.size()]);
		length = Math.min(lautoinsuranceInsurance_Policys.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lautoinsuranceInsurance_Policys[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Insurance_Policy_Payment...");
		java.util.List lInsurance_Policy_PaymentList = autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Insurance_Policy_Payment").setMaxResults(ROW_COUNT).list();
		autoinsurance.Insurance_Policy_Payment[] lautoinsuranceInsurance_Policy_Payments = (autoinsurance.Insurance_Policy_Payment[]) lInsurance_Policy_PaymentList.toArray(new autoinsurance.Insurance_Policy_Payment[lInsurance_Policy_PaymentList.size()]);
		length = Math.min(lautoinsuranceInsurance_Policy_Payments.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lautoinsuranceInsurance_Policy_Payments[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Insurance_Product...");
		java.util.List lInsurance_ProductList = autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Insurance_Product").setMaxResults(ROW_COUNT).list();
		autoinsurance.Insurance_Product[] lautoinsuranceInsurance_Products = (autoinsurance.Insurance_Product[]) lInsurance_ProductList.toArray(new autoinsurance.Insurance_Product[lInsurance_ProductList.size()]);
		length = Math.min(lautoinsuranceInsurance_Products.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lautoinsuranceInsurance_Products[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Insurance_Quote...");
		java.util.List lInsurance_QuoteList = autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Insurance_Quote").setMaxResults(ROW_COUNT).list();
		autoinsurance.Insurance_Quote[] lautoinsuranceInsurance_Quotes = (autoinsurance.Insurance_Quote[]) lInsurance_QuoteList.toArray(new autoinsurance.Insurance_Quote[lInsurance_QuoteList.size()]);
		length = Math.min(lautoinsuranceInsurance_Quotes.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lautoinsuranceInsurance_Quotes[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Insured_Driver...");
		java.util.List lInsured_DriverList = autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Insured_Driver").setMaxResults(ROW_COUNT).list();
		autoinsurance.Insured_Driver[] lautoinsuranceInsured_Drivers = (autoinsurance.Insured_Driver[]) lInsured_DriverList.toArray(new autoinsurance.Insured_Driver[lInsured_DriverList.size()]);
		length = Math.min(lautoinsuranceInsured_Drivers.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lautoinsuranceInsured_Drivers[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Policyholder...");
		java.util.List lPolicyholderList = autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Policyholder").setMaxResults(ROW_COUNT).list();
		autoinsurance.Policyholder[] lautoinsurancePolicyholders = (autoinsurance.Policyholder[]) lPolicyholderList.toArray(new autoinsurance.Policyholder[lPolicyholderList.size()]);
		length = Math.min(lautoinsurancePolicyholders.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lautoinsurancePolicyholders[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Vehicle...");
		java.util.List lVehicleList = autoinsurance.AutoinsurancePersistentManager.instance().getSession().createQuery("From autoinsurance.Vehicle").setMaxResults(ROW_COUNT).list();
		autoinsurance.Vehicle[] lautoinsuranceVehicles = (autoinsurance.Vehicle[]) lVehicleList.toArray(new autoinsurance.Vehicle[lVehicleList.size()]);
		length = Math.min(lautoinsuranceVehicles.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lautoinsuranceVehicles[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
	}
	
	public static void main(String[] args) {
		try {
			ListAutoinsuranceData listAutoinsuranceData = new ListAutoinsuranceData();
			try {
				listAutoinsuranceData.listTestData();
			}
			finally {
				autoinsurance.AutoinsurancePersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
