package ui;

import autoinsurance.Customer;
import autoinsurance.Vehicle;
import org.metaworks.annotation.Face;
import org.metaworks.annotation.Hidden;
import org.metaworks.annotation.ServiceMethod;

import java.util.ArrayList;
import java.util.List;

public class VehiclesTab {

    Customer customer;
    private java.util.List<autoinsurance.Vehicle> vehicles;

    public VehiclesTab() {
        setVehicles(new ArrayList<Vehicle>());
    }

    @Hidden
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Face(faceClass = VehicleListFace.class)
    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }


    @ServiceMethod(callByContent = true)
    public DriversTab next() {
        for (Vehicle vehicle : getVehicles()) {
            if (vehicle.isSelected()) {
                DriversTab driversTab = new DriversTab(customer, vehicle);

                return driversTab;
            }
        }

        throw new RuntimeException("Please Select one or more vehicle to go next.");
    }

    @ServiceMethod
    public NameAndAddressTab previous() {
        // TODO - implement VehiclesTab.previous
        throw new UnsupportedOperationException();
    }

}