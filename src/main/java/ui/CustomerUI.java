package ui;


import org.metaworks.annotation.ServiceMethod;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class CustomerUI extends autoinsurance.Customer{

    @ServiceMethod(callByContent = true)
    public void save() throws PersistentException {
        PersistentTransaction t = autoinsurance.AutoinsurancePersistentManager.instance().getSession().beginTransaction();

        try{

            autoinsurance.Customer customer = new autoinsurance.Customer();

            customer.setCustomer_ID(getCustomer_ID());
            customer.setCustomer_first_name(getCustomer_first_name());


           autoinsurance.AutoinsurancePersistentManager.instance().getSession().save(customer);

            t.commit();
        }
        catch (Exception e) {

            t.rollback();

            throw e;
        }


    }

}
