package ui;

import autoinsurance.AutoinsurancePersistentManager;
import autoinsurance.Customer;
import autoinsurance.Policyholder;
import autoinsurance.Vehicle;
import org.metaworks.annotation.Face;
import org.metaworks.annotation.Order;
import org.metaworks.annotation.ServiceMethod;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class DriversTab {

    private Policyholder policyholder;
    private Vehicle vehicle;

    public DriversTab() {
        setPolicyholder(new Policyholder());
    }

    public DriversTab(Customer customer, Vehicle vehicle) {
        setPolicyholder((Policyholder) customer);
        setVehicle(vehicle);
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Order(1)
    @Face(ejsPath = "dwr/metaworks/autoinsurance/Policyholder_Drivers.ejs")
    public Policyholder getPolicyholder() {
        return this.policyholder;
    }

    public void setPolicyholder(Policyholder policyholder) {
        this.policyholder = policyholder;
    }

    @ServiceMethod(callByContent = true)
    public FinalDetailsTab next() throws PersistentException {
        PersistentTransaction transaction = autoinsurance.AutoinsurancePersistentManager.instance().getSession().beginTransaction();

        try {
            AutoinsurancePersistentManager.instance().getSession().save(getPolicyholder());

            transaction.commit();
        } catch (PersistentException e) {
            transaction.rollback();
            throw e;
        }

        FinalDetailsTab tab = new FinalDetailsTab();
        tab.setCustomer(getPolicyholder());

        return tab;
    }

    @ServiceMethod
    public VehiclesTab previous() {
        // TODO - implement VehiclesTab.previous
        throw new UnsupportedOperationException();
    }

}