package ui;

import autoinsurance.Customer;
import org.metaworks.annotation.ServiceMethod;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class CustomerRegistration {



    public Customer customer;

        public Customer getCustomer() {
            return customer;
        }

        public void setCustomer(Customer customer) {
            this.customer = customer;
        }


    @ServiceMethod(callByContent = true)
    public void save() throws PersistentException {
        PersistentTransaction t = autoinsurance.AutoinsurancePersistentManager.instance().getSession().beginTransaction();

        try{


            autoinsurance.AutoinsurancePersistentManager.instance().getSession().save(getCustomer());

            t.commit();
        }
        catch (Exception e) {

            t.rollback();

            throw e;
        }


    }

}
