package ui;

import autoinsurance.Policyholder;
import org.metaworks.annotation.Face;
import org.metaworks.annotation.Order;
import org.metaworks.annotation.ServiceMethod;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class NameAndAddressTab {

    private autoinsurance.Policyholder policyholder;
    private String disclosure;

    public NameAndAddressTab() {
        setPolicyholder(new Policyholder());
    }

    @Order(3)
    @ServiceMethod(callByContent = true)
    public VehiclesTab startMyQoute() throws PersistentException {
        // TODO - implement NameAndAddressTab.startMyQoute
        //throw new UnsupportedOperationException();
        //    Policyholder p = new Policyholder();

        PersistentTransaction t = autoinsurance.AutoinsurancePersistentManager.instance().getSession().beginTransaction();

        try {
//            p.setCustomer_first_name(getPolicyholder().getCustomer_first_name());
//            p.setCustomer_social_security_number(getPolicyholder().getCustomer_social_security_number());
//            p.setCustomer_ID(getPolicyholder().getCustomer_ID());

            autoinsurance.AutoinsurancePersistentManager.instance().getSession().save(getPolicyholder());

            t.commit();
        } catch (Exception e) {

            t.rollback();

            throw e;
        }

        VehiclesTab vehiclesTab = new VehiclesTab();
        vehiclesTab.setCustomer(getPolicyholder());

        return vehiclesTab;
    }


    @Order(2)
    public String getDisclosure() {
        return this.disclosure;
    }

    /**
     * @param disclosure
     */
    public void setDisclosure(String disclosure) {
        this.disclosure = disclosure;
    }

    @Order(1)
    @Face(ejsPath = "dwr/metaworks/autoinsurance/Policyholder_NameAndAddress.ejs")
    public Policyholder getPolicyholder() {
        return this.policyholder;
    }

    /**
     * @param policyholder
     */
    public void setPolicyholder(Policyholder policyholder) {
        this.policyholder = policyholder;
    }

}