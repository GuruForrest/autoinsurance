package ui;

import autoinsurance.Customer;
import autoinsurance.Policyholder;

public class FinalDetailsTab {

    private Customer customer;
    private Policyholder policyholder;

    public FinalDetailsTab() {
        setPolicyholder(new Policyholder());
    }

    public Policyholder getPolicyholder() {
        return policyholder;
    }

    public void setPolicyholder(Policyholder policyholder) {
        this.policyholder = policyholder;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}